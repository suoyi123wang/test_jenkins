package com.yd.elastic.service;

public interface OrderService {



    boolean addConsumeOrder(String start,String end);

    boolean addRechargeOrder(String start,String end);

    boolean addRefundOrder(String start,String end);
}
