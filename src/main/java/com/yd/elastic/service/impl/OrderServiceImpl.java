package com.yd.elastic.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yd.elastic.domain.ConsumOrder;
import com.yd.elastic.domain.RechargeOrder;
import com.yd.elastic.domain.RefundOrder;
import com.yd.elastic.repository.ConsumOrderRepository;
import com.yd.elastic.repository.RechargeOrderRepository;
import com.yd.elastic.repository.RefundOrderRepository;
import com.yd.elastic.service.IndexService;
import com.yd.elastic.service.OrderService;
import com.yd.elastic.utils.ConstantsSearch;
import com.yd.elastic.utils.JpaUtils;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    IndexService indexService;

    @Autowired
    EntityManager entityManager;

    @Autowired
    ConsumOrderRepository consumOrderRepository;

    @Autowired
    RechargeOrderRepository rechargeOrderRepository;

    @Autowired
    RefundOrderRepository refundOrderRepository;

    @Override
    @Transactional
    public boolean addRechargeOrder(String start, String end) {
        List<RechargeOrder> list =rechargeOrderRepository.getListRechargeOrder(start,end);
        String json = JSON.toJSONString(list);
        JSONArray array = JSONArray.parseArray(json);
        indexService.batchInsert(ConstantsSearch.ORDER_RECHARGE_INDEX,"_doc",array);
        return true;
    }

    @Override
    public boolean addConsumeOrder(String start, String end) {
        List<ConsumOrder> list =consumOrderRepository.getListConsumeOrder(start,end);
        String json = JSON.toJSONString(list);
        JSONArray array = JSONArray.parseArray(json);
        indexService.batchInsert(ConstantsSearch.ORDER_CONSUME_INDEX,"_doc",array);
        return true;
    }

    @Override
    public boolean addRefundOrder(String start, String end) {
        List<RefundOrder> list =refundOrderRepository.getListRefundOrder(start,end);
        String json = JSON.toJSONString(list);
        JSONArray array = JSONArray.parseArray(json);
        indexService.batchInsert(ConstantsSearch.ORDER_REFUND_INDEX,"_doc",array);
        return true;
    }
}
