package com.yd.elastic.service.impl;

import com.yd.elastic.service.SearchService;
import com.yd.elastic.utils.ConstantsSearch;
import com.yd.elastic.utils.req.RequestVo;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private TransportClient transportClient;

    @Override
    public List<Map> getAmountRecharge(RequestVo requestVo){
        SearchRequestBuilder searchRequestBuilder = transportClient
                .prepareSearch(ConstantsSearch.ORDER_RECHARGE_INDEX)
                .setSize(9999);

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must(QueryBuilders.rangeQuery("pay_time")
                .format("epoch_millis")
                .from("")
                .timeZone(TimeZone.getDefault().getID())
                .includeLower(true));
        return null;
    }

    @Override
    public void aggregationRefund(String index, String type, RequestVo request) {
        SearchRequestBuilder searchRequestBuilder = transportClient.prepareSearch(index);

        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        if (request.getPartnerId() != null && request.getPartnerId() != "") {
            qb.must(QueryBuilders.termQuery("partner_id", request.getPartnerId()));
        }
        if (request.getRegionId() != null && request.getRegionId() != "") {
            qb.must(QueryBuilders.termQuery("region_id", request.getRegionId()));
        }
        if (request.getAreaId() != null && request.getAreaId() != "") {
            qb.must(QueryBuilders.termQuery("area_id", request.getAreaId()));
        }

        if (request.getBeginDate() != null && request.getEndDate() != "") {
            qb.must(QueryBuilders.rangeQuery("create_time").timeZone("CTT").format("yyyy-MM-dd HH:mm:ss").from(request.getBeginDate()).to(request.getEndDate()));
        }


        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(qb);
        searchRequestBuilder.setSource(searchSourceBuilder);
//
//        SumAggregationBuilder sumRefundAmount=AggregationBuilders.sum("sumAmount").field("refund_amount").field("region_id");
//        TermsAggregationBuilder termPartner= AggregationBuilders.terms("partnerGroup").field("partner_id");
//        TermsAggregationBuilder termRegion= AggregationBuilders.terms("regionGroup").field("region_id");
//        TermsAggregationBuilder termArea= AggregationBuilders.terms("areaGroup").field("area_id");

//        termArea.subAggregation(termRegion);
//        termRegion.subAggregation(sumRefundAmount);

//
//
//        searchRequestBuilder.addAggregation(termRegion);
//        searchRequestBuilder.addAggregation(termRegion);
//
        SearchResponse response = searchRequestBuilder.setSize(1000).execute().actionGet();

        SearchHits hits = response.getHits();
        System.out.println("----------------------");
//sout
//        Map<String, Aggregation> map=response.getAggregations().getAsMap();
//        map.get("sumAmount").getMetaData();

    }
}
