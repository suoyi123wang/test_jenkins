package com.yd.elastic.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yd.elastic.service.IndexService;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class IndexServiceImpl implements IndexService {


    private static final Logger LOGGER = LoggerFactory.getLogger(IndexServiceImpl.class);

    @Autowired
    private TransportClient client;

    @Override
    public boolean createIndex(String index) {
        if (!isIndexExist(index)) {
            LOGGER.info("Index is not exits!");
        }
        CreateIndexResponse indexresponse = client.admin().indices().prepareCreate(index).execute().actionGet();
        LOGGER.info("执行建立成功？" + indexresponse.isAcknowledged());

        return indexresponse.isAcknowledged();
    }

    @Override
    public boolean isIndexExist(String index) {
        IndicesExistsResponse inExistsResponse = client.admin().indices().exists(new IndicesExistsRequest(index)).actionGet();
        if (inExistsResponse.isExists()) {
            LOGGER.info("Index [" + index + "] is exist!");
        } else {
            LOGGER.info("Index [" + index + "] is not exist!");
        }
        return inExistsResponse.isExists();

    }

    @Override
    public boolean createIndexWithSetting(String index, Integer shardNum, Integer replicaNum,Integer maxResult) {

        if (!isIndexExist(index)) {
            LOGGER.info("Index is not exits!");
        }
        CreateIndexResponse indexresponse = client.admin().indices().prepareCreate(index)
                .setSettings(Settings.builder()
                        .put("index.number_of_shards", shardNum)
                        .put("index.number_of_replicas", replicaNum)
                        .put("index.max_result_window",maxResult)
                )
                .get();
        LOGGER.info("执行建立成功？" + indexresponse.isAcknowledged());
        return indexresponse.isAcknowledged();

    }

    @Override
    public boolean deleteIndex(String index) {

        boolean deleteIndex= client.admin().indices().prepareDelete(index).execute().actionGet().isAcknowledged();
        return deleteIndex;

    }

    @Override
    public boolean putMapping(String index, String type, String mapping) {

        boolean putMapping=client.admin().indices().preparePutMapping(index).setType(type).setSource(mapping, XContentType.JSON).get().isAcknowledged();
        return putMapping;
    }

    @Override
    public void deleteDataById(String index, String type, String id) {

        DeleteResponse response = client.prepareDelete(index, type, id).execute().actionGet();
        LOGGER.info("deleteDataById response status:{},id:{}", response.status().getStatus(), response.getId());
    }

    @Override
    public void updateDataById(JSONObject jsonObject, String index,  String id) {

        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index(index).id(id).doc(jsonObject);
        client.update(updateRequest);

    }

    @Override
    public String addData(JSONObject jsonObject, String index) {
        IndexResponse response = client.prepareIndex(index, "_doc").setSource(jsonObject).get();
        LOGGER.info("addData response status:{},id:{}", response.status().getStatus(), response.getId());
        return response.getId();
    }

    @Override
    public void batchInsert(String index,String type, JSONArray jsonArray) {
        BulkRequestBuilder bulkRequest = client.prepareBulk();
       if(jsonArray.size()>0){
           for(int i=0;i<jsonArray.size();i++){
               bulkRequest.add(client.prepareIndex(index, type).setSource((Map)jsonArray.get(i)));
           }
       }
        bulkRequest.execute().actionGet();
        LOGGER.info("批量插入完成");
    }

}
