package com.yd.elastic.service;

import com.yd.elastic.utils.req.RequestVo;

import java.util.List;
import java.util.Map;

public interface SearchService {

    List<Map> getAmountRecharge(RequestVo requestVo);

    void aggregationRefund(String index, String type, RequestVo request);
}
