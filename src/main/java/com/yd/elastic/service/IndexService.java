package com.yd.elastic.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


import java.util.List;
import java.util.Map;

public interface IndexService {

    boolean createIndex(String index);

    boolean isIndexExist(String index);

    boolean createIndexWithSetting(String index,Integer shardNum ,Integer replicaNum,Integer maxResult);

    boolean deleteIndex(String index);

    boolean putMapping(String index, String type,String mapping);

    void deleteDataById(String index, String type, String id);

    void updateDataById(JSONObject jsonObject, String index,  String id);

    String addData(JSONObject jsonObject, String index);

    void batchInsert(String index, String type, JSONArray jsonArray);





}
