package com.yd.elastic.repository;

import com.yd.elastic.domain.ConsumOrder;
import com.yd.elastic.domain.RechargeOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RechargeOrderRepository extends JpaRepository<RechargeOrder,String> {
    @Query(value = "SELECT * FROM uu_business_center.t_order_recharge WHERE create_time >= ?1 and create_time < ?2",nativeQuery = true)
    List<RechargeOrder> getListRechargeOrder(String start, String end);
}
