package com.yd.elastic.repository;

import com.yd.elastic.domain.RefundOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RefundOrderRepository extends JpaRepository<RefundOrder,String> {
    @Query(value = "SELECT * FROM uu_business_center.t_refund_list WHERE create_time >= ?1 and create_time < ?2",nativeQuery = true)
    List<RefundOrder> getListRefundOrder(String start, String end);
}
