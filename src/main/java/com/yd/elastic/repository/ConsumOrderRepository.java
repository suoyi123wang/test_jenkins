package com.yd.elastic.repository;

import com.yd.elastic.domain.ConsumOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ConsumOrderRepository extends JpaRepository<ConsumOrder,String> {

    @Query(value = "SELECT * FROM uu_business_center.t_order_consumption WHERE create_time >= ?1 and create_time < ?2",nativeQuery = true)
    List<ConsumOrder> getListConsumeOrder(String start,String end);
}
