package com.yd.elastic.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="t_order_recharge")
public class RechargeOrder {
    @Id   //标明主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String order_id;

    private String out_order_id;
    private String out_transaction_id;
    private String device_id;
    private String device_nickname;
    private String device_type;
    private String stall_id;
    private Integer stall_type;
    private String product_id;
    private String partner_id;
    private String region_id;
    private String area_id;
    private String shop_id;
    private String user_id;
    private String out_user_id;
    private Integer pay_channel;
    private Integer pay_type;
    private Integer pay_status;
    private Integer amounts;
    private Integer points;
    private Integer coin;
    private Integer play_num;
    private Date create_time;
    private Date update_time;
    private Date pay_time;
    private Integer is_valid;
    private String bus_order_id;
    private String ext_info;
    private String ext_info_bak;
    private Integer promo_limit;
    private Integer recharge_points;
    private String nickname;

    public RechargeOrder() { }

    public String getOrder_id() {
        return order_id;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public String getOut_transaction_id() {
        return out_transaction_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getDevice_nickname() {
        return device_nickname;
    }

    public String getDevice_type() {
        return device_type;
    }

    public String getStall_id() {
        return stall_id;
    }

    public Integer getStall_type() {
        return stall_type;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public String getArea_id() {
        return area_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getOut_user_id() {
        return out_user_id;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public Integer getPay_type() {
        return pay_type;
    }

    public Integer getPay_status() {
        return pay_status;
    }

    public Integer getAmounts() {
        return amounts;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getCoin() {
        return coin;
    }

    public Integer getPlay_num() {
        return play_num;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public Date getPay_time() {
        return pay_time;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public String getBus_order_id() {
        return bus_order_id;
    }

    public String getExt_info() {
        return ext_info;
    }

    public String getExt_info_bak() {
        return ext_info_bak;
    }

    public Integer getPromo_limit() {
        return promo_limit;
    }

    public Integer getRecharge_points() {
        return recharge_points;
    }

    public String getNickname() {
        return nickname;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }

    public void setOut_transaction_id(String out_transaction_id) {
        this.out_transaction_id = out_transaction_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setDevice_nickname(String device_nickname) {
        this.device_nickname = device_nickname;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public void setStall_id(String stall_id) {
        this.stall_id = stall_id;
    }

    public void setStall_type(Integer stall_type) {
        this.stall_type = stall_type;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setOut_user_id(String out_user_id) {
        this.out_user_id = out_user_id;
    }

    public void setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
    }

    public void setPay_type(Integer pay_type) {
        this.pay_type = pay_type;
    }

    public void setPay_status(Integer pay_status) {
        this.pay_status = pay_status;
    }

    public void setAmounts(Integer amounts) {
        this.amounts = amounts;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }

    public void setPlay_num(Integer play_num) {
        this.play_num = play_num;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public void setPay_time(Date pay_time) {
        this.pay_time = pay_time;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    public void setBus_order_id(String bus_order_id) {
        this.bus_order_id = bus_order_id;
    }

    public void setExt_info(String ext_info) {
        this.ext_info = ext_info;
    }

    public void setExt_info_bak(String ext_info_bak) {
        this.ext_info_bak = ext_info_bak;
    }

    public void setPromo_limit(Integer promo_limit) {
        this.promo_limit = promo_limit;
    }

    public void setRecharge_points(Integer recharge_points) {
        this.recharge_points = recharge_points;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
