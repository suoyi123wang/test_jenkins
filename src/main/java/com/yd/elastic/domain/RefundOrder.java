package com.yd.elastic.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="t_refund_list")
public class RefundOrder {
    @Id   //标明主键
    private String id;

    private String order_id;
    private String out_order_id;
    private String out_trade_number;
    private Integer refund_type;
    private Integer refund_way;
    private Integer refund_sub_way;
    private Integer order_source;
    private Integer pay_channel;
    private Integer pay_type;
    private String user_id;
    private String user_nickname;
    private Integer amounts;
    private Integer points;
    private Integer refund_amount;
    private Integer deduct_points;
    private Integer before_points;
    private Integer status;
    private String device_id;
    private String device_nickname;
    private String product_id;
    private String partner_id;
    private String region_id;
    private String area_id;
    private String shop_id;
    private String remark;
    private Integer created_platfrom;
    private String created_by;
    private Date create_time;
    private String updated_by;
    private Date update_time;
    private Integer is_valid;

    public String getId() {
        return id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public String getOut_trade_number() {
        return out_trade_number;
    }

    public Integer getRefund_type() {
        return refund_type;
    }

    public Integer getRefund_way() {
        return refund_way;
    }

    public Integer getRefund_sub_way() {
        return refund_sub_way;
    }

    public Integer getOrder_source() {
        return order_source;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public Integer getPay_type() {
        return pay_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public Integer getAmounts() {
        return amounts;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getRefund_amount() {
        return refund_amount;
    }

    public Integer getDeduct_points() {
        return deduct_points;
    }

    public Integer getBefore_points() {
        return before_points;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getDevice_nickname() {
        return device_nickname;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public String getArea_id() {
        return area_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public String getRemark() {
        return remark;
    }

    public Integer getCreated_platfrom() {
        return created_platfrom;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }

    public void setOut_trade_number(String out_trade_number) {
        this.out_trade_number = out_trade_number;
    }

    public void setRefund_type(Integer refund_type) {
        this.refund_type = refund_type;
    }

    public void setRefund_way(Integer refund_way) {
        this.refund_way = refund_way;
    }

    public void setRefund_sub_way(Integer refund_sub_way) {
        this.refund_sub_way = refund_sub_way;
    }

    public void setOrder_source(Integer order_source) {
        this.order_source = order_source;
    }

    public void setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
    }

    public void setPay_type(Integer pay_type) {
        this.pay_type = pay_type;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }

    public void setAmounts(Integer amounts) {
        this.amounts = amounts;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public void setRefund_amount(Integer refund_amount) {
        this.refund_amount = refund_amount;
    }

    public void setDeduct_points(Integer deduct_points) {
        this.deduct_points = deduct_points;
    }

    public void setBefore_points(Integer before_points) {
        this.before_points = before_points;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setDevice_nickname(String device_nickname) {
        this.device_nickname = device_nickname;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setCreated_platfrom(Integer created_platfrom) {
        this.created_platfrom = created_platfrom;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }
}
