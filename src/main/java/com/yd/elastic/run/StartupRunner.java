package com.yd.elastic.run;


import com.yd.elastic.service.IndexService;
import com.yd.elastic.utils.ConstantsSearch;
import org.elasticsearch.client.transport.TransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(1)
@Component
public class StartupRunner implements CommandLineRunner {


    @Autowired
    TransportClient transportClient;

    @Autowired
    IndexService indexService;

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupRunner.class);

    @Override
    public void run(String... args) throws Exception {

        LOGGER.info(">>>>>>>>>>>>>>>服务启动执行，执行建立索引<<<<<<<<<<<<<");

        if(!indexService.isIndexExist(ConstantsSearch.ORDER_RECHARGE_INDEX)){
            LOGGER.info(">>>>>>>>>>>>>>>执行建立索引<<<<<<<<<<<<<"+ConstantsSearch.ORDER_RECHARGE_INDEX);
            indexService.createIndexWithSetting(ConstantsSearch.ORDER_RECHARGE_INDEX,ConstantsSearch.SHARD_NUM,ConstantsSearch.REPLICA,ConstantsSearch.MAX_RESULT);
            indexService.putMapping(ConstantsSearch.ORDER_RECHARGE_INDEX,"_doc",ConstantsSearch.ORDER_RECHARGE_MAPPING);
        }
        if(!indexService.isIndexExist(ConstantsSearch.ORDER_CONSUME_INDEX)){
            LOGGER.info(">>>>>>>>>>>>>>>执行建立索引<<<<<<<<<<<<<"+ConstantsSearch.ORDER_RECHARGE_INDEX);
            indexService.createIndexWithSetting(ConstantsSearch.ORDER_CONSUME_INDEX,ConstantsSearch.SHARD_NUM,ConstantsSearch.REPLICA,ConstantsSearch.MAX_RESULT);
            indexService.putMapping(ConstantsSearch.ORDER_CONSUME_INDEX,"_doc",ConstantsSearch.ORDER_CONSUME_MAPPING);
        }
        if(!indexService.isIndexExist(ConstantsSearch.ORDER_REFUND_INDEX)){
            LOGGER.info(">>>>>>>>>>>>>>>执行建立索引<<<<<<<<<<<<<"+ConstantsSearch.ORDER_RECHARGE_INDEX);
            indexService.createIndexWithSetting(ConstantsSearch.ORDER_REFUND_INDEX,ConstantsSearch.SHARD_NUM,ConstantsSearch.REPLICA,ConstantsSearch.MAX_RESULT);
            indexService.putMapping(ConstantsSearch.ORDER_REFUND_INDEX,"_doc",ConstantsSearch.ORDER_REFUND_MAPPING);
        }
    }
}
