package com.yd.elastic.utils;

public class ConstantsSearch {

    public static final Integer SHARD_NUM=5;

    public static final Integer REPLICA=1;

    public static final Integer MAX_RESULT=100000;

    public static final String ORDER_RECHARGE_INDEX="order_recharge_index";

    public static final String ORDER_CONSUME_INDEX="order_consume_index";

    public static final String ORDER_REFUND_INDEX="order_refund_index";


    public static final String ORDER_RECHARGE_MAPPING="{\"properties\":" +
            "{\"order_id\":{\"type\":\"keyword\"}," +
            "\"out_order_id\":{\"type\":\"keyword\"}," +
            "\"out_transaction_id\":{\"type\":\"keyword\"}," +
            "\"device_id\":{\"type\":\"keyword\"}," +
            "\"device_nickname\":{\"type\":\"keyword\"}," +
            "\"device_type\":{\"type\":\"keyword\"}," +
            "\"stall_id\":{\"type\":\"keyword\"}," +
            "\"stall_type\":{\"type\":\"keyword\"}," +
            "\"product_id\":{\"type\":\"keyword\"}," +
            "\"partner_id\":{\"type\":\"keyword\"}," +
            "\"region_id\":{\"type\":\"keyword\"}," +
            "\"area_id\":{\"type\":\"keyword\"}," +
            "\"shop_id\":{\"type\":\"keyword\"}," +
            "\"user_id\":{\"type\":\"keyword\"}," +
            "\"out_user_id\":{\"type\":\"keyword\"}," +
            "\"pay_channel\":{\"type\":\"integer\"}," +
            "\"pay_type\":{\"type\":\"integer\"}," +
            "\"pay_status\":{\"type\":\"integer\"}," +
            "\"amounts\":{\"type\":\"integer\"}," +
            "\"points\":{\"type\":\"integer\"}," +
            "\"coin\":{\"type\":\"integer\"}," +
            "\"play_num\":{\"type\":\"integer\"}," +
            "\"create_time\":{\"type\":\"date\"}," +
            "\"update_time\":{\"type\":\"date\"}," +
            "\"pay_time\":{\"type\":\"date\"}," +
            "\"is_valid\":{\"type\":\"integer\"}," +
            "\"bus_order_id\":{\"type\":\"text\"}," +
            "\"ext_info\":{\"type\":\"text\"}," +
            "\"ext_info_bak\":{\"type\":\"text\"}," +
            "\"promo_limit\":{\"type\":\"integer\"}," +
            "\"recharge_points\":{\"type\":\"integer\"}," +
            "\"nickname\":{\"type\":\"text\"}}}";

    public static final String ORDER_CONSUME_MAPPING="{\"properties\":" +
            "{\"order_id\":{\"type\":\"keyword\"}," +
            "\"out_order_id\":{\"type\":\"keyword\"}," +
            "\"out_transaction_id\":{\"type\":\"keyword\"}," +
            "\"device_id\":{\"type\":\"keyword\"}," +
            "\"device_nickname\":{\"type\":\"keyword\"}," +
            "\"device_type\":{\"type\":\"keyword\"}," +
            "\"stall_id\":{\"type\":\"keyword\"}," +
            "\"stall_type\":{\"type\":\"keyword\"}," +
            "\"product_id\":{\"type\":\"keyword\"}," +
            "\"partner_id\":{\"type\":\"keyword\"}," +
            "\"region_id\":{\"type\":\"keyword\"}," +
            "\"area_id\":{\"type\":\"keyword\"}," +
            "\"shop_id\":{\"type\":\"keyword\"}," +
            "\"user_id\":{\"type\":\"keyword\"}," +
            "\"out_user_id\":{\"type\":\"keyword\"}," +
            "\"points\":{\"type\":\"integer\"}," +
            "\"coin\":{\"type\":\"integer\"}," +
            "\"run_num\":{\"type\":\"integer\"}," +
            "\"order_status\":{\"type\":\"integer\"}," +
            "\"cb_url\":{\"type\":\"text\"}," +
            "\"success_time\":{\"type\":\"date\"}," +
            "\"create_time\":{\"type\":\"date\"}," +
            "\"update_time\":{\"type\":\"date\"}," +
            "\"is_valid\":{\"type\":\"integer\"}," +
            "\"order_source\":{\"type\":\"integer\"}," +
            "\"created_by\":{\"type\":\"text\"}," +
            "\"updated_by\":{\"type\":\"text\"}," +
            "\"nickname\":{\"type\":\"text\"}}}";


    public static final String ORDER_REFUND_MAPPING="{\"properties\":" +
            "{\"id\":{\"type\":\"keyword\"}," +
            "\"order_id\":{\"type\":\"keyword\"}," +
            "\"out_order_id\":{\"type\":\"keyword\"}," +
            "\"out_trade_number\":{\"type\":\"keyword\"}," +
            "\"refund_type\":{\"type\":\"integer\"}," +
            "\"refund_way\":{\"type\":\"integer\"}," +
            "\"refund_sub_way\":{\"type\":\"integer\"}," +
            "\"order_source\":{\"type\":\"integer\"}," +
            "\"pay_channel\":{\"type\":\"integer\"}," +
            "\"pay_type\":{\"type\":\"integer\"}," +
            "\"user_id\":{\"type\":\"keyword\"}," +
            "\"user_nickname\":{\"type\":\"keyword\"}," +
            "\"amounts\":{\"type\":\"integer\"}," +
            "\"points\":{\"type\":\"integer\"}," +
            "\"refund_amount\":{\"type\":\"integer\"}," +
            "\"deduct_points\":{\"type\":\"integer\"}," +
            "\"before_points\":{\"type\":\"integer\"}," +
            "\"status\":{\"type\":\"integer\"}," +
            "\"device_id\":{\"type\":\"keyword\"}," +
            "\"device_nickname\":{\"type\":\"keyword\"}," +
            "\"product_id\":{\"type\":\"keyword\"}," +
            "\"partner_id\":{\"type\":\"keyword\"}," +
            "\"region_id\":{\"type\":\"keyword\"}," +
            "\"area_id\":{\"type\":\"keyword\"}," +
            "\"shop_id\":{\"type\":\"keyword\"}," +
            "\"remark\":{\"type\":\"text\"}," +
            "\"created_platfrom\":{\"type\":\"integer\"}," +
            "\"created_by\":{\"type\":\"text\"}," +
            "\"create_time\":{\"type\":\"date\"}," +
            "\"updated_by\":{\"type\":\"text\"}," +
            "\"update_time\":{\"type\":\"date\"}," +
            "\"is_valid\":{\"type\":\"integer\"}}}";

}
