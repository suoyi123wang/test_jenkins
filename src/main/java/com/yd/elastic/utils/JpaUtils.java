package com.yd.elastic.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JpaUtils {

    public static <T> List<T> listMapToObject(List<Map> objects,Class<T> clazz){
        String str = JSON.toJSONString(objects);
        return JSONObject.parseArray(str,clazz);
    }

    public static <T> List<T> listConvert(List<T> objects,Class<T> clazz){
        String str = JSON.toJSONString(objects);
        return JSONObject.parseArray(str,clazz);
    }

    public static <T> T singleMapToObject(Map objects, Class<T> clazz) {
        String str = JSON.toJSONString(objects);
        return JSONObject.parseObject(str,clazz);
    }

    public static String convertSetToString(Set<String> strset){
        List<String> strlist = new ArrayList<>(strset);
        StringBuffer sb = new StringBuffer();
        if(strlist.size()>0){
            for (int i=0;i<strlist.size();i++) {
                if(i==0){
                    sb.append("'").append(strlist.get(i)).append("'");
                }else{
                    sb.append(",").append("'").append(strlist.get(i)).append("'");
                }
            }
        }
        return sb.toString();
    }

    public static String convertListToString(List<String> strlist){
//        List<String> strlist = new ArrayList<>(strset);
        StringBuffer sb = new StringBuffer();
        if(strlist.size()>0){
            for (int i=0;i<strlist.size();i++) {
                if(i==0){
                    sb.append("'").append(strlist.get(i)).append("'");
                }else{
                    sb.append(",").append("'").append(strlist.get(i)).append("'");
                }
            }
        }
        return sb.toString();
    }

}
