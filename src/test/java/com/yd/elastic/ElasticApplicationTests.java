package com.yd.elastic;


import com.yd.elastic.service.IndexService;

import com.yd.elastic.service.OrderService;
import com.yd.elastic.service.SearchService;
import com.yd.elastic.utils.ConstantsSearch;
import com.yd.elastic.utils.req.RequestVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticApplicationTests {


	@Autowired
	IndexService indexService;

	@Test
	public void contextLoads() {
	}

	@Autowired
	OrderService orderService;

	@Autowired
	SearchService searchService;

	@Test
	public void test1() {
		orderService.addConsumeOrder("2019-08-01 00:00:00","2019-08-02 00:00:00");
		orderService.addRefundOrder("2019-08-01 00:00:00","2019-08-02 00:00:00");
		orderService.addRechargeOrder("2019-08-01 00:00:00","2019-08-02 00:00:00");
		System.out.println("succcess");
	}

	@Test
	public void test2() {
		RequestVo requestVo = new RequestVo();
		requestVo.setBeginDate("2019-08-01 00:00:00");
		requestVo.setEndDate("2019-08-02 00:00:00");
		requestVo.setPartnerId("201807039060");
		searchService.aggregationRefund(ConstantsSearch.ORDER_REFUND_INDEX,"_doc",requestVo);
	}

}
